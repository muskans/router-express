const express = require("express");
const app = express();

// basic http get request on '/' 
app.get('/get', function(req, res)
{
    res.send("Hello World!");
});

// server port listen on 3000
app.listen(3001, function() {
    console.log('Server is running on port 3001');
});

