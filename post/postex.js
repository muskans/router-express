const express = require("express");
const app = express();

// basic http get request on '/' 
app.post('/post', function(req, res)
{
    res.send("Hello World!");
});

// server port listen on 3000
app.listen(3002, function() {
    console.log('Server is running on port 3002 for post method');
});

